package br.ufrn.usoba.servicos;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import br.ufrn.usoba.configuracao.DetalheUsuario;
import br.ufrn.usoba.dominio.Usuario;
import br.ufrn.usoba.excecoes.UsuarioNaoEncontradoException;
import br.ufrn.usoba.repositorio.UsuarioRepository;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DetalheUsuarioService implements UserDetailsService	{
	
	private UsuarioRepository usuarioRepository;
	
	@Override
	public DetalheUsuario loadUserByUsername(String userName) throws UsuarioNaoEncontradoException {
		Usuario usuario = usuarioRepository.findByEmail(userName).orElseThrow(() -> new UsuarioNaoEncontradoException());
		DetalheUsuario detalheUsuario = new DetalheUsuario(usuario);
		return detalheUsuario;
	}

}
