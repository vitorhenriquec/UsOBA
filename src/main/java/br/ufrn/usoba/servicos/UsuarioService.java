package br.ufrn.usoba.servicos;

import java.util.List;

import org.springframework.stereotype.Service;

import br.ufrn.usoba.dominio.Usuario;
import br.ufrn.usoba.excecoes.UsuarioNaoEncontradoException;
import br.ufrn.usoba.repositorio.UsuarioRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UsuarioService {
	private UsuarioRepository usuarioRepository;
	
	public List<Usuario> buscarTodos() {
		return this.usuarioRepository.findAll();
	}
	
	public Usuario buscarUsuario(Long idUsuario) throws UsuarioNaoEncontradoException {
		return usuarioRepository.findById(idUsuario).orElseThrow(() -> new UsuarioNaoEncontradoException());
	}
}
