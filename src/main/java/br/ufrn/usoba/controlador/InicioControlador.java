package br.ufrn.usoba.controlador;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.usoba.configuracao.DetalheUsuario;
import br.ufrn.usoba.excecoes.CredenciaisInvalidasException;
import br.ufrn.usoba.servicos.DetalheUsuarioService;
import br.ufrn.usoba.util.JwtUtil;
import br.ufrn.usoba.util.RespostaAutenticacao;
import br.ufrn.usoba.util.SolicitacaoAutenticacao;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class InicioControlador {
	private AuthenticationManager authenticationManager;
	
	private DetalheUsuarioService detalheUsuarioService;
	
	private JwtUtil jwtUtil;
	
	@PostMapping("/autenticar")
	@Transactional(readOnly=true)
	public ResponseEntity<?> autenticar(@RequestBody SolicitacaoAutenticacao authenticationRequest) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getSenha()));
		}catch(CredenciaisInvalidasException e) {
			return new ResponseEntity<>(CredenciaisInvalidasException.msg, HttpStatus.BAD_REQUEST);
		}
		
		final DetalheUsuario detalheUsuario = (DetalheUsuario) detalheUsuarioService.loadUserByUsername(authenticationRequest.getEmail());
		final String jwt = jwtUtil.generateToken(detalheUsuario);
		
		return ResponseEntity.ok(new RespostaAutenticacao(jwt));
	}
}
