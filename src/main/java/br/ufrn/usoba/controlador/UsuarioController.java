package br.ufrn.usoba.controlador;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufrn.usoba.servicos.UsuarioService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/usuario")
public class UsuarioController {
	
	private UsuarioService usuarioService;
	
	@PreAuthorize("hasRole('PAPEL_ADMIN')")
	@GetMapping
	@Transactional(readOnly=true)
	public ResponseEntity<?> buscarTodos() {
		return ResponseEntity.ok().body(this.usuarioService.buscarTodos());
	}
}
