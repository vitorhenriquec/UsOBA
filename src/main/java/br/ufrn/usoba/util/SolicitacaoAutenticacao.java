package br.ufrn.usoba.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SolicitacaoAutenticacao {
	private String email;
	
	private String senha;
}
