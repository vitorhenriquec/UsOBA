package br.ufrn.usoba.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RespostaAutenticacao {
	private final String jwt;
}
