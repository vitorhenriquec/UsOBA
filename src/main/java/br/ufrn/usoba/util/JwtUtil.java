package br.ufrn.usoba.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.ufrn.usoba.configuracao.DetalheUsuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtUtil {
	@Value("${jwt.secret}")
	private String SECRET_KEY;
	
	@Value("${jwt.expirationTime}")
	private long expirationTime; 
	
	public String extractUsername(String token) {
		return extractClaim(token, Claims::getSubject);
	}
	
	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}
	
	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllCaims(token);
		return claimsResolver.apply(claims);
	}
	
	private Claims extractAllCaims(String token) {
		return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
	}
	
	private Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}
	
	public String generateToken(DetalheUsuario detalheUsuario) {
		Map<String, Object> claims = new HashMap<>();
		return createToken(claims, detalheUsuario.getUsername());
	}

	private String createToken(Map<String, Object> claims, String username) {
		return Jwts.builder().setClaims(claims).setSubject(username)
				.setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + expirationTime*60)).signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	}
	
	public Boolean validateToken(String token, DetalheUsuario detalheUsuario) {
		final String username = extractUsername(token);
		return (username.equals(detalheUsuario.getUsername()) && !isTokenExpired(token));
	}
}
