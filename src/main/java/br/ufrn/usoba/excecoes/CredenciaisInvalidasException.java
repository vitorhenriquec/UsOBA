package br.ufrn.usoba.excecoes;

import org.springframework.security.authentication.BadCredentialsException;

@SuppressWarnings("serial")
public class CredenciaisInvalidasException extends BadCredentialsException{
	public static final String msg = "Credências incorretas";
	
	public CredenciaisInvalidasException() {
		super(msg);
	}

}
