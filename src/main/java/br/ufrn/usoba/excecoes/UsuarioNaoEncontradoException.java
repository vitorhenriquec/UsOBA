package br.ufrn.usoba.excecoes;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SuppressWarnings("serial")
public final class UsuarioNaoEncontradoException extends UsernameNotFoundException{
	
	public static final String msg = "Usuário não encontrado";
	
	public UsuarioNaoEncontradoException() {
		super(msg);
	}

}
