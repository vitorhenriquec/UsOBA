package br.ufrn.usoba.dominio;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/*
 * Enum para mapear e guardar as informações sobre perfis
 * 
 * @autor Vitor Bezerra
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum Papel {
	ADMIN(1, "ADMIN"), REVISOR(2, "REVISOR"), PADRAO(3, "PADRAO"), PESQUISADOR(4, "PESQUISADOR"),
	REVISOR_DOUTOR(5, "REVISOR_DOUTOR");

	private int id;

	private String denominacao;

	private static List<Papel> perfis = new ArrayList<>();

	static {
		perfis.add(Papel.ADMIN);
		perfis.add(Papel.REVISOR);
		perfis.add(Papel.PADRAO);
		perfis.add(Papel.PESQUISADOR);
		perfis.add(Papel.REVISOR_DOUTOR);
	}

	public static List<Papel> getPerfis() {
		return perfis;
	}

}
