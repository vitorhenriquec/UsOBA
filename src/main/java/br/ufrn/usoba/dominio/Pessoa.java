package br.ufrn.usoba.dominio;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Entity(name="pessoa")
@Data
@Table(
		name="pessoa",
		uniqueConstraints = {
				@UniqueConstraint(name="pessoa_email", columnNames="email")
		}
)
@EqualsAndHashCode(of = { "id" })
@RequiredArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED) 
//Usuário do obama pode ser um participante? Pra que serve a token do participante? 
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@Pattern(regexp = "^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)$")
	private String email;
		
	@Column(name = "momento_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private LocalDateTime momentoCriacao = LocalDateTime.now();
	
	@Column(name = "data_modificao")
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private LocalDateTime dataModificao;
}
