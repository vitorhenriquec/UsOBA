package br.ufrn.usoba.dominio;

import lombok.AllArgsConstructor;
import lombok.Getter;

/*
 * Enum para mapear tipos de cadastro
 * 
 * @autor Vitor Bezerra
 */

@Getter
@AllArgsConstructor
public enum TipoCadastro {

	PADRAO("Padrao"), GOOGLE("Google"), FACEBOOK("Facebook");

	private String chave;
}
