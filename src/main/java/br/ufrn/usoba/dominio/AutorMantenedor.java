package br.ufrn.usoba.dominio;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*
 * Classe de domínio para mapear e guardar os autores mantenedores
 * 
 * @autor Vitor Bezerra
 */

@Entity(name="AutoMantenedor")
@Data
@Table(
		name="autor_mantenedor",
		uniqueConstraints = {
				@UniqueConstraint(name="autor_site", columnNames="site")
		}
)
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
public class AutorMantenedor extends Pessoa{

	private static final long serialVersionUID = 1L;
	
	private String site;
	
}
