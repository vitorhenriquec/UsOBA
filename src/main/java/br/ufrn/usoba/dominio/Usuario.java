package br.ufrn.usoba.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

/*
 * Classe de domínio para mapear e guardar os usuários 
 * 
 * @autor Vitor Bezerra
 */

@Entity(name="Usuario")
@Data
@Table(name="usuario")
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper=false)
@PrimaryKeyJoinColumn(name="id")
@JsonIgnoreProperties({"senha"})
public class Usuario extends Pessoa {

	private static final long serialVersionUID = 1L;
	
	private String senha;

	private boolean ativo;
	
	@Enumerated(EnumType.STRING)
	private Papel papel;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_cadastro")
	private TipoCadastro tipoCadastro;
}
